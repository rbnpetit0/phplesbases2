<?php

// Création d'une liste vide
$list = [];

// Récupérer la fonction de l'ex03 qui prend une chaine de caractères en
// argument, et renvoie un tableau trié des différents mots)

function ft_split($input)
{
    if (preg_split("/[\s]+/", $input, -1, PREG_SPLIT_NO_EMPTY) != false) {
        $tableau = preg_split("/[\s]+/", $input, -1, PREG_SPLIT_NO_EMPTY);

        return $tableau;
    }
}

// récupere le tableau
$tableau = array_slice($argv, 1);
// Fonction de tri

// je crée une boucle avec le nombre d'élèments
for ($i = 1; $i < $argc; ++$i) {
    foreach (ft_split($argv[$i]) as $mot) {
        // je créé un tableau
        array_push($list, $mot);
    }
}

 // je trie tous les mots de mon tableau dans un ordre logique

$tableau_numeric = [];
$tableau_lettres_a_z = [];
$tableau_trash = [];
// j'appelle chaque valeur dans mon tableau et je la renvoie
foreach ($list as $value) {
    // if (preg_match('/\b[[:alpha:]]/', $value) == 1) {
    if (ctype_alpha($value) === true) {
        $tableau_lettres_a_z[] = $value;
    } elseif (is_numeric($value) === true) {
        $tableau_numeric[] = $value;
    } else {
        $tableau_trash[] = $value;
    }
}
natcasesort($tableau_lettres_a_z);

foreach ($tableau_lettres_a_z as $letters) {
    echo "$letters\n";
}
sort($tableau_numeric, SORT_STRING);
foreach ($tableau_numeric as $chiffre) {
    echo "$chiffre\n";
}
sort($tableau_trash);
foreach ($tableau_trash as $trash) {
    echo "$trash\n";
}
// boucle avec affichage standart des mots
